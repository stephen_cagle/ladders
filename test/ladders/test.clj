(ns ladders.test
  (:require
   [clojure.test :as test]
   [ladders.core :as core]
   [ladders.givens :as givens]))

(def sample-emails (take (* 100) givens/emails))

(def valid-emails (core/process sample-emails))

(defn last-100 [xs d]
  (let [new-xs (conj xs d)
        c (count new-xs)]
    (subvec new-xs (max 0 (- c 100)))))

(test/testing "Rules state that"

  (test/testing "we send at most 1 email to each address"
    (test/is (= (-> (map :email-address valid-emails) set count)
                (count valid-emails))))

  (test/testing "we must never send a email with a spam rating above 0.3"
    (doseq [{:keys [spam-score] :as email} valid-emails]
      (test/is (<= spam-score 0.3) email)))

  (test/testing "the average spam score must never be above 0.05"
    (doseq [{:keys [average-spam-score] :as email} valid-emails]
      (test/is (<= average-spam-score 0.05) email)))

  (test/testing "the most recent 100 emails must have mean spam score below 0.1"
    (doseq [emails (reductions last-100 [] valid-emails)]
      (let [scores (map :spam-score emails)
            average-score (apply + scores)]
        (test/is (< average-score 0.1) {:first (first emails) :last (last emails)})))))

