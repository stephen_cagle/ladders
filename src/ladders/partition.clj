(ns ladders.partition
  (:require
   [ladders.io :as io]))

(defn first-character-of-email [d]
  (-> d :email-address first))

(defn partition-to-files [partitioner stream]
  (let [files (atom {})]
    (doseq [o (io/read-seq-from-file stream)
            :let [bucket (partitioner o)]]
      (when-not (contains? @files bucket)
        (let [file (io/temp-file)]
          (swap! files #(assoc % bucket file))))
      (io/write! (@files bucket) o))
    @files))
