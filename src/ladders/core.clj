(ns ladders.core
  (:require
   [ladders.givens :as givens]))

(defn ratio [number]
  (clojure.lang.Numbers/toRatio (rationalize number)))

(defn add-history [email prev-email]
  (let [{:keys [spam-score]} email
        {:keys [history average-spam-score size]} prev-email
        conjed-history (conj history spam-score)
        new-history (if (= (count conjed-history) 101)
                      (subvec conjed-history 1)
                      conjed-history)
        new-size (inc size)
        recency-spam-score (/ (apply + new-history) (ratio (count new-history)))
        new-average-spam-score (/ (+ (* average-spam-score size) spam-score) new-size)]
    (if (and (<= recency-spam-score 1/10) (<= new-average-spam-score 1/20))
      (assoc email :average-spam-score new-average-spam-score :history new-history :size new-size)
      prev-email)))

(defn history-reduction-fx [emails email]
  (let [prev-email (last emails)
        new-email (add-history email prev-email)]
    (if (= new-email prev-email)
      emails
      (conj emails new-email))))

(defn process [emails]
  (->> emails
       (filter #(< (:spam-score %) 0.3)) ;; take out anything too spamy
       (map #(update-in % [:spam-score] ratio)) ;; not dealing with floating point weirdness
       ;; (reductions history-reduction-fx [{:history [] :size 0/1 :average-spam-score 0/1 :remove-this? true}])
       ;; last
       ;; (map #(dissoc % :history))
       last
       println
        ;; drop the first item of reductions (one with {:remove-this? true}
       
))

