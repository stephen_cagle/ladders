(ns ladders.io
  (:require
   [ladders.givens :as givens]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn abs-path [file]
  (.getAbsolutePath file))

(defn temp-file
  ([] (temp-file (uuid)))
  ([prefix] (temp-file prefix ".clj"))
  ([prefix suffix]
   (doto (java.io.File/createTempFile prefix suffix)
     (.deleteOnExit))))

(defn write! [stream data]
  (with-open [w (clojure.java.io/writer stream :append true)]
    (binding [*out* w]
      (pr data))))

;; https://github.com/clojure-cookbook/clojure-cookbook/blob/master/04_local-io/4-14_read-write-clojure-data-structures.asciidoc
(defn- read-one
  [r]
  (try
    (read r)
    (catch java.lang.RuntimeException e
      (if (= "EOF while reading" (.getMessage e))
        ::EOF
        (throw e)))))

(defn read-seq-from-file
  "Reads a sequence of top-level objects in file at path."
  [stream]
  (with-open [r (java.io.PushbackReader. (clojure.java.io/reader stream))]
    (binding [*read-eval* false]
      (doall (take-while #(not= ::EOF %) (repeatedly #(read-one r)))))))

(defn test-file
  ([] (test-file (* 10 1000)))
  ([size] (test-file (temp-file) size))
  ([stream size]
   (let [datas (take size givens/emails)]
     (doseq [data datas]
       (write! stream datas))
     stream)))
