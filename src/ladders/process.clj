(ns ladders.process
  (:require
   [ladders.partition :as ladders.partition]
   [ladders.io :as ladders.io]))

(defn dedup-and-merge [emails]
  (let [spam-scores (map :spam-score emails)
        max-spam-score (apply max spam-scores)]
    (assoc (first emails) :spam-score max-spam-score)))

(defn spamy? [{:keys [spam-score]}]
  (<= 0.3 spam-score))

(defn process-emails [emails]
  (let [stream (ladders.io/temp-file)]
    (doseq [email (->> emails
                       (sort-by :email-address)
                       (partition-by :email-address)
                       (map dedup-and-merge)
                       (remove spamy?)
                       (sort-by :spam-score))]
      (ladders.io/write! stream email))
    stream))

(defn files->process-files [files]
  (reduce
   (fn [d [k file]]
     (let [emails (ladders.io/read-seq-from-file file)]
       (assoc d k (process-emails emails))))
   {}
   files))

(defn email-seq-by-spam-score [xs]
  (-> xs first :spam-score))

(defn spam-sorted [xs]
  (->> xs
       (sort-by email-seq-by-spam-score)
       (drop-while empty?)))

(defn take-next-email [{:keys [streams]}]
  (let [[email & emails] (first streams)]
    {:email email
     :streams (->> streams rest (cons emails) spam-sorted)}))

(defn combine-files [files]
  (let [streams (->> files
                     vals
                     (map ladders.io/read-seq-from-file)
                     spam-sorted)]
    (->> (iterate take-next-email {:streams streams})
         rest ;; iterate returns x, (f x), (f (f x)) ...
         (take-while #(-> % :streams empty? not)))))

(defn ratio [number]
  (clojure.lang.Numbers/toRatio (rationalize number)))

(defn map-mean-spam-score-fx []
  (fn [ds]
    (let [mean-spam-score (atom 0/1)]
      (-> (fn [{:keys [email position] :as d}]
            (let [{:keys [spam-score]} email
                  spam-score (ratio spam-score)]
              (reset! mean-spam-score (/ (+ (* @mean-spam-score position) spam-score) (inc position)))
              (assoc d :mean-spam-score @mean-spam-score)))
          (map ds)))))

(defn run [input-file output-file]
  (let [map-mean-spam-score (map-mean-spam-score-fx)
        emails (->> input-file
                    (ladders.partition/partition-to-files ladders.partition/first-character-of-email)
                    files->process-files
                    combine-files
                    (map-indexed #(assoc %2 :position %1))
                    map-mean-spam-score
                    (take-while #(<= (:mean-spam-score %) 1/20))
                    (map :email))]
    (doseq [email emails]
      (ladders.io/write! output-file email))))

(defn test-run
  ([] (test-run 1000))
  ([samples]
   (let [input-file (ladders.io/test-file samples)
         output-file (ladders.io/temp-file)]
     (run input-file output-file)
     output-file)))
