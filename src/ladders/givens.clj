(ns ladders.givens
  (:require
   [clojure.spec :as spec]
   [clojure.spec.gen :as gen]))

(def email-domains
  #{"indeediot.com"
    "monstrous.com"
    "linkedarkpattern.com"
    "dired.com"
    "lice.com"
    "careershiller.com"
    "glassbore.com"})

(def email-regex
  #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")

(spec/def ::email-address
  (spec/with-gen
    (spec/and string? #(re-matches email-regex %))
    #(->>
      (gen/tuple (gen/such-that not-empty (gen/string-alphanumeric))
                 (spec/gen email-domains))
      (gen/fmap (fn [[addr domain]] (str addr "@" domain))))))

(spec/def ::spam-score
  (spec/double-in :min 0 :max 1))

(spec/def ::email-record
  (spec/keys :req-un [::email-address ::spam-score]))

(def emails (gen/sample (spec/gen ::email-record) (* 10 1000 1000 1000)))
