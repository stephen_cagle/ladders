# Spam Code Submission

## Intro

The following is my solution to the spam code problem. The goal is to 
send out the maximum number of emails possible without being marked as spam.
The rules are as follows:

1. Must send a maximum of one email per address.
2. Must never send an email with a spam score of more than 0.3.
3. The running mean of spam ratings must never get above 0.05. (In
   other words, as each email is processed the mean spam score of all
   the emails that have sent in the batch needs to be recalculated and
   can't ever be more than 0.05.)
4. The mean spam score of the most recent 100 emails sent can't go
   above 0.1.
   
## Solution

My first solution was a simple sequence processing solution. However, this fails
to scale as I was unable to sort on such a large sample space of emails (millions).
I believed sorting to be neccessary as rule 1 requires that you never send an email
twice to the same address. Sorting was necessary to dedup the emails. I also operate
on the assumption that if the same address is associated with multiple spam scores 
then I pick the highest spam score for that address.

My second (this) pass involved simply bucketing the list of emails to a memory manageable 
size such that I could perform sequence operations on the buckets and serialize and 
deserialize them to disk. My bucketing algorithm is simply the first character of the 
email address. Future work might involve creating a more uniformly distributed (hash
based) bucketing algorithm. For this case, first character is sufficient. Within each of 
these buckets, I dedup (and combine to max :spam-score) all the emails, filter out all 
emails > 0.3, and then sort the emails by their :spam score.

Now I have N (first character of :email-address) buckets that have been serialized to disk. 
I then combine all of these N buckets (files) into a single file. I do so by keeping a list 
of all the open files in :spam-score sorted order. By taking one email out of the lowest spam
bucket in each iteration of the algorithm and writting it to disk, I end up with a final 
file that contains all of the emails in :spam-score sorted order. Every email also 
has its running score calculated and the whole write is `(take-while)`'ed' so that the scores have
a mean score below 0.05 . 

## Failures

I didn't specifically program rule #4. I don't think it would have been hard, I think I could split
the final file into two sequences, one reading from the begining (lowest score) and one from the
top (highest score). Basically read from the high scores until you get to the point that you are
going to break the rule #4 invariant, then read from the bottom to "balance" it out. Keep doing 
this until the top index and bottom index cross. Then close the file and you are done.

Simply, I did not get to this, I have already worked long enough on this challenge. :/

## Uncertainties

My goal is to maximize the total number of emails sent. With that said, I had some trouble
figuring out whether the above algorithm actualy guarantees this. I know if I am adding in emails
in their :spam sorted order, then that is absolutely the maximum number of emails that can 
possibly be sent. With that said, I am not sure under what (if any) conditions rule 4 would kick
in to cause fewer emails to be sent. I found that hard to reason about. 

At every iteration when merging all the files into a single sorted file, I am sorting (NLog(N)) 
all the opened files. This could be improved dramatically by doing a binary search and swap on 
the opened files rather than just re-sorting every time. Didn't have time for that.

I would like to have had time to implement a better bucketing algorithm, hashing would allow for
efficient bucketing independent of distribution of hashed value.

I attempted to make everything lazy that I could (especially file io). With that said, I am not 
sure if I  succeeded at this, my runtimes seems pretty linear and I do not appear to be using a huge 
amount of memory dependent on size, so I may have succeeded.

~~~~
ladders.process> (time (def result (test-run (* 1000))))
"Elapsed time: 381.458552 msecs"

ladders.process> (time (def result (test-run (* 10 1000))))
"Elapsed time: 3634.656408 msecs"

ladders.process> (time (def result (test-run (* 100 1000))))
"Elapsed time: 36738.339065 msecs"

ladders.process> (time (def result (test-run (* 1000 1000))))
"Elapsed time: 444716.938351 msecs"
#'ladders.process/result
~~~~

I suspect that there are probably actual libraries for doing this sort of "big scale" sequence 
processing that would make all of this much much easier. For something that I have 1 day to work
upon I though exploring those libraries would not be worthwhile, but they might have saved me time.

I didn't have time to do a whole lot of testing.

I just didn't have time to do many things that I would have liked to do. (That's life :] )

## Usage

Unfortunately, the interface is pretty poor. I am at the end of the day and simply do not have the 
time to refine it further. Easiest thing to do is to simply switch to the `ladders.process` ns and 
run `(def result (test-run 10000))`. This will return you a result file that should contain a list of 
emails that you can send out. You can read the objects in this file using `(ladders.io/read-seq-from-file result)`,
be careful, it might bust your repl if you attempt to read them all.

Again, apologies for the lousy (non-existent) interface.
