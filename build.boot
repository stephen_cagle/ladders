(set-env!
 :source-paths #{"src" "test"}
 :dependencies
 '[[adzerk/boot-reload                      "0.5.2" :scope "test"]
   [adzerk/boot-test                        "1.2.0" :scope "test"]
   [org.clojure/clojure                     "1.9.0-alpha14"]
   [org.clojure/test.check                  "0.10.0-alpha3" :scope "test"]
   [org.clojure/tools.cli                   "0.3.7"]])

(require '[adzerk.boot-test :refer :all])
